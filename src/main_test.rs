use super::Rover;
use super::Direction;
use super::Movement;

#[test]
fn with_direction() {
    let rover = Rover {
        x: 1,
        y: 1,
        dir: Direction::North
    };
    let west_rover = rover.with_direction(Direction::West);
    assert_eq!(1, west_rover.x);
    assert_eq!(1, west_rover.y);
    assert_eq!(Direction::West, west_rover.dir);
}

#[test]
fn with_position() {
    let rover = Rover {
        x: 2,
        y: 3,
        dir: Direction::North
    };
    let moved_rover = rover.with_position((5, 4));
    assert_eq!(5, moved_rover.x);
    assert_eq!(4, moved_rover.y);
    assert_eq!(Direction::North, moved_rover.dir);
}

#[test]
fn turn_left() {
    let mut rover = Rover { x: 2, y: 3, dir: Direction::North };
    assert_eq!(rover.do_move(Movement::Left).dir, Direction::West);
    rover = Rover { x: 2, y: 3, dir: Direction::West };
    assert_eq!(rover.do_move(Movement::Left).dir, Direction::South);
    rover = Rover { x: 2, y: 3, dir: Direction::South };
    assert_eq!(rover.do_move(Movement::Left).dir, Direction::East);
    rover = Rover { x: 2, y: 3, dir: Direction::East };
    assert_eq!(rover.do_move(Movement::Left).dir, Direction::North);
}

#[test]
fn turn_right() {
    let mut rover = Rover { x: 2, y: 3, dir: Direction::North };
    assert_eq!(rover.do_move(Movement::Right).dir, Direction::East);
    rover = Rover { x: 2, y: 3, dir: Direction::West };
    assert_eq!(rover.do_move(Movement::Right).dir, Direction::North);
    rover = Rover { x: 2, y: 3, dir: Direction::South };
    assert_eq!(rover.do_move(Movement::Right).dir, Direction::West);
    rover = Rover { x: 2, y: 3, dir: Direction::East };
    assert_eq!(rover.do_move(Movement::Right).dir, Direction::South);
}

#[test]
fn advance() {
    let mut rover = Rover { x: 2, y: 3, dir: Direction::North };
    assert_eq!(rover.do_move(Movement::Forward).coords(), (2, 4));
    rover = Rover { x: 2, y: 3, dir: Direction::West };
    assert_eq!(rover.do_move(Movement::Forward).coords(), (1, 3));
    rover = Rover { x: 2, y: 3, dir: Direction::South };
    assert_eq!(rover.do_move(Movement::Forward).coords(), (2, 2));
    rover = Rover { x: 2, y: 3, dir: Direction::East };
    assert_eq!(rover.do_move(Movement::Forward).coords(), (3, 3));
}