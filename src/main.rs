#[derive(PartialEq,Debug)]
pub enum Direction { North, East, South, West }

pub enum Movement { Left, Forward, Right }

impl Direction {

    fn turn_left(self) -> Direction {
        match self {
            Direction::North => Direction::West,
            Direction::West => Direction::South,
            Direction::South => Direction::East,
            Direction::East => Direction::North
        }
    }

    fn turn_right(self) -> Direction {
        match self {
            Direction::North => Direction::East,
            Direction::East => Direction::South,
            Direction::South => Direction::West,
            Direction::West => Direction::North
        }
    }

    fn turn(self, movement: Movement) -> Direction {
        match movement {
            Movement::Left => self.turn_left(),
            Movement::Right => self.turn_right(),
            _ => self
        }
    }

    fn advance(&self, pos: (u32, u32)) -> (u32, u32) {
        let (x, y) = pos;
        match self {
            &Direction::North => (x, y + 1),
            &Direction::South => (x, y - 1),
            &Direction::West  => (x - 1, y),
            &Direction::East  => (x + 1, y)
        }
    }

}

pub struct Rover {
    x: u32,
    y: u32,
    dir: Direction
}

impl Rover {

    fn turn(self, movement: Movement) -> Rover {
        Rover {
            x: self.x,
            y: self.y,
            dir: self.dir.turn(movement)
        }
    }

    fn forward(self) -> Rover {
        let (new_x, new_y) = self.dir.advance((self.x, self.y));
        Rover {
            x: new_x,
            y: new_y,
            dir: self.dir
        }
    }

    pub fn coords(&self) -> (u32, u32) {
        (self.x, self.y)
    }
    
    pub fn do_move(self, movement: Movement) -> Rover {
        match movement {
            Movement::Left    => self.turn(movement),
            Movement::Right   => self.turn(movement),
            Movement::Forward => self.forward()
        }
    }

    pub fn with_direction(self, dir: Direction) -> Rover {
        Rover {
            x: self.x,
            y: self.y,
            dir: dir
        }
    }
    
    pub fn with_position(self, pos: (u32, u32)) -> Rover {
        let (x, y) = pos;
        Rover {
            x: x,
            y: y,
            dir: self.dir
        }
    }
}

#[cfg(not(test))]
fn main() {
    let input = vec![
        "5 5",
        "1 2 N",
        "LRLLMMRRMM"
    ];
}

#[cfg(test)]
mod main_test;